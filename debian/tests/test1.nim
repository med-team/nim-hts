import hts

# open a bam/cram and look for the index.
var b:Bam
open(b, "HG02002.bam", index=true, fai="aa.fa")

for record in b:
  if record.mapping_quality > 10u:
    echo record.chrom, record.start, record.stop

# regional queries:
for record in b.query("6", 30816675, 32816675):
  if record.flag.proper_pair and record.flag.reverse:
    # cigar is an iterable of operations:
    for op in record.cigar:
      # $op gives the string repr of the operation, e.g. '151M'
      echo $op, " ", op.consumes.reference, " ", op.consumes.query

    # tags are pulled by type `ta`
    var mismatches = tag[int](record, "NM")
    if not mismatches.isNone and mismatches.get < 3:
      var rg = tag[string](record, "RG")
      if not rg.isNone: echo rg.get

