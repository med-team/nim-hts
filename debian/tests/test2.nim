import hts

var tsamples = @["101976-101976", "100920-100920", "100231-100231", "100232-100232", "100919-100919"]
# VCF and BCF supported
var v:VCF
doAssert(open(v, "test.bcf", samples=tsamples))

var afs = new_seq[float32](5) # size doesn't matter. this will be re-sized as needed
var acs = new_seq[int32](5) # size doesn't matter. this will be re-sized as needed
var csq = new_string_of_cap(20)
for rec in v:
  echo rec, " qual:", rec.QUAL, " filter:", rec.FILTER
  var info = rec.info
  # accessing stuff from the INFO field is meant to be as fast as possible, allowing
  # the user to re-use memory as needed.
  doAssert info.get("CSQ", csq) == Status.OK # string
  doAssert info.get("AC", acs) == Status.OK # ints
  doAssert info.get("AF", afs) == Status.OK # floats
  echo acs, afs, csq, info.has_flag("IN_EXAC")

  # accessing format fields is similar
  var dps = new_seq[int32](len(v.samples))
  doAssert rec.format.get("DP", dps) == Status.OK

# open a VCF for writing
var wtr:VCF
doAssert(open(wtr, "outv.vcf", mode="w"))
wtr.header = v.header
doAssert(wtr.write_header())

# regional queries look for index. works for VCF and BCF
for rec in v.query("1:15600-18250"):
  echo rec.CHROM, ":", $rec.POS
  # adjust some values in the INFO
  var val = 22.3
  doAssert rec.info.set("VQSLOD", val) == Status.OK
  doAssert wtr.write_variant(rec)

